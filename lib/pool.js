var client = require('../node_modules/easymysql');
var pool = require('../config/db_config').pool;
var connections = [];

for(var k in  pool){
	
	var conn = client.create({
		'maxconnections':10,
	});
	
	conn.addserver({
		'host':pool[k].host,
		'user':pool[k].user,
		'password':pool[k].password,
		'db':pool[k].db,
	});
	
	connections[k] = conn;
	
}

exports.connections = connections;
exports.pool = pool;