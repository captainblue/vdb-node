module.exports.conf = require('./config/config');
var http = require('http');
var rrest = require('./node_modules/rrestjs');

//http server
server = http.createServer(function(req,res){	
	try{
		require('./controller/vdb').parse(req,res);
	}
	catch(err){
		
		res.end(' '+err);
	}
}).listen(rrest.config.listenPort);
